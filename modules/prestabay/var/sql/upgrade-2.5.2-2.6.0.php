<?php


$installer = $this;
$installer->executeSql(
    "
    ALTER TABLE `PREFIX_prestabay_profiles` 
      ADD COLUMN `business_policy_payment` VARCHAR(20) NULL,
      ADD COLUMN `business_policy_return`   VARCHAR(20) NULL,
      ADD COLUMN `business_policy_shipping` VARCHAR(20) NULL;
      
      
");

Configuration::set('INVEBAY_STATUS_NOT_COMPLETE', Configuration::get('PS_OS_CHEQUE'));
Configuration::set('INVEBAY_STATUS_PAYMENT_PENDING', Configuration::get('PS_OS_CHEQUE'));
Configuration::set('INVEBAY_STATUS_PAYMENT_PEND_PP', Configuration::get('PS_OS_PAYPAL'));
Configuration::set('INVEBAY_STATUS_PAYMENT_FAILED', Configuration::get('PS_OS_ERROR'));
Configuration::set('INVEBAY_STATUS_PAYMENT_COMPLETE', Configuration::get('PS_OS_PAYMENT'));
Configuration::set('INVEBAY_STATUS_SHIPPING_COMPLETE', Configuration::get('PS_OS_SHIPPING'));