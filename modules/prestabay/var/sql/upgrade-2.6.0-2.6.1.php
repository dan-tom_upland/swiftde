<?php


$installer = $this;
$installer->executeSql(
    "
    ALTER TABLE `PREFIX_prestabay_order_items`
      ADD COLUMN `final_fee_value` float DEFAULT NULL;

");
