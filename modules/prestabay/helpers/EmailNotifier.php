<?php


class EmailNotifier
{
    public static function sendEmailAlert($info, $additionalData = array())
    {
        if (!Configuration::get('INVEBAY_ALERT_ENABLE', false)) {
            return false;
        }

        $toEmail = Configuration::get('INVEBAY_ALERT_EMAIL');

        if (empty($toEmail)) {
            return false;
        }

        return Mail::Send(
            (int) (Configuration::get('PS_LANG_DEFAULT')),
            'info',
            Mail::l('PrestaBay Automatic Notification'),
            array(
                '{info}' => $info,
                '{additionalData}' => var_export($additionalData, true),
            ),
            $toEmail,
            'Store owner',
            null, // from
            null, // from name
            null, // file attachment
            null, // mode smtp
            _PS_MODULE_DIR_ . 'prestabay/views/mails/'
        );
    }
}