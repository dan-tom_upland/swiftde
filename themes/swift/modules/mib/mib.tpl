<div class="block" id="mypresta_mib">
  <p class="title_block">{l s='Our Manufacturers' mod='mib'}<a href="/hersteller" class="hidden-xs">{l s="Alles sehen"  mod="mib"}  <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>

  <div class="manuContent">
    {hook h="displayManufacturer"}
    <ul>
      {capture}{$manufacturers|@shuffle}{/capture}
      {foreach from=$manufacturers item=manufacturer name=manufacturer_list}
      {if $manufacturer.image}

      {if $smarty.foreach.manufacturer_list.index == 6} {break}{/if}

      <li class="{if $smarty.foreach.manufacturer_list.last}last_item{elseif $smarty.foreach.manufacturer_list.first}first_item{else}item{/if}">
       <a href="{$link->getmanufacturerLink($manufacturer.id_manufacturer, $manufacturer.link_rewrite)|escape:'html'}" title="{l s='More about %s' sprintf=[$manufacturer.name] mod='mib'}">
        <img src="{$content_dir}img/m/{$manufacturer.image_url}" alt="{$manufacturer.name|escape:'html':'UTF-8'}" class="img-responsive" />
      </a>
    </li>
    {/if}
    {/foreach}
  </ul>
</div>
</div>