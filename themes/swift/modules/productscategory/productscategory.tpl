{*
	* 2007-2016 PrestaShop
	*
	* NOTICE OF LICENSE
	*
	* This source file is subject to the Academic Free License (AFL 3.0)
	* that is bundled with this package in the file LICENSE.txt.
	* It is also available through the world-wide-web at this URL:
	* http://opensource.org/licenses/afl-3.0.php
	* If you did not receive a copy of the license and are unable to
	* obtain it through the world-wide-web, please send an email
	* to license@prestashop.com so we can send you a copy immediately.
	*
	* DISCLAIMER
	*
	* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
	* versions in the future. If you wish to customize PrestaShop for your
	* needs please refer to http://www.prestashop.com for more information.
	*
	*  @author PrestaShop SA <contact@prestashop.com>
	*  @copyright  2007-2016 PrestaShop SA
	*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
	*  International Registered Trademark & Property of PrestaShop SA
	*}
	{if count($categoryProducts) > 0 && $categoryProducts !== false}
	<section class="page-product-box blockproductscategory">
		<p class="title_block">
			{l s="Products in the same category" mod="productscategory"}
		</p> 
		<div id="productscategory_list" class="clearfix">
			<ul id="bxslider1" class="bxslider clearfix product_list grid">
				{foreach from=$categoryProducts item='categoryProduct' name=categoryProduct}
				<li class="product-box item">
					<div class="product-container">
						<div class="left-block">
							<a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)}" class="lnk_img product-image" title="{$categoryProduct.name|htmlspecialchars}"><img src="{$link->getImageLink($categoryProduct.link_rewrite, $categoryProduct.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$categoryProduct.name|htmlspecialchars}" /></a>



							{assign var="pImages" value=Product::geImagesByID($product.id_product, 1)}{foreach from=$pImages item=image name=images}
							<img src="{$link->getImageLink($product.link_rewrite, $image, 'home_default')}" {if $smarty.foreach.images.first}class="current replace-2x img-responsive second img_{$smarty.foreach.images.index}"{else} class="img_{$smarty.foreach.images.index}" style="display:none;"{/if} alt="{$product.legend|escape:'htmlall':'UTF-8'}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if}/>
							{/foreach}


						</div>
						<div class="right-block">
							<h5 itemprop="name" class="product-name">
								<a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)|escape:'html':'UTF-8'}" title="{$categoryProduct.name|htmlspecialchars}">{$categoryProduct.name|truncate:60:'...'|escape:'html':'UTF-8'}</a>
							</h5>









							<div class="button-containerr">


								<div class="showFirst">
									<div class="leftSide">



										<div class="content_price">
											


											{if $ProdDisplayPrice && $categoryProduct.show_price == 1 && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
											<p class="price_display">
												{if isset($categoryProduct.specific_prices) && $categoryProduct.specific_prices
												&& ($categoryProduct.displayed_price|number_format:2 !== $categoryProduct.price_without_reduction|number_format:2)}

												<span class="price special-price">{convertPrice price=$categoryProduct.displayed_price}</span>
												
												

												{else}
												<span class="price">{convertPrice price=$categoryProduct.displayed_price}</span>
												{/if}
											</p>
											<div class="brutto">{l s="Inkl. MwSt." mod="productscategory"}

												{if $categoryProduct.specific_prices.reduction && $categoryProduct.specific_prices.reduction_type == 'percentage'}
												<span class="old-price">{displayWtPrice p=$categoryProduct.price_without_reduction}</span>
												{/if}
											</div>
											{/if}



										</div>
									</div>

									<div class="rightSide">



										{if !$PS_CATALOG_MODE && ($categoryProduct.allow_oosp || $categoryProduct.quantity > 0)}

										<span>

											{if $categoryProduct.features[0].id_feature == 75}
											{$categoryProduct.features[0].value}
											{else}
											{l s="Versand 24 Uhr" mod="productscategory"}
											{/if}

										</span>
										
										<a class="ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$categoryProduct.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$categoryProduct.id_product|intval}" title="{l s='Add to cart' mod='productscategory'}">
											<img src="{$img_dir}/custom/20-icon-product.svg"/ alt="{l s='Add to cart'}">
										</a>

										{else}
										<span>{l s="Nicht verfügbar" mod="productscategory"}</span>

										<a class="ajax_add_to_cart_button disabled">
											<img src="{$img_dir}/custom/10-icon-product.svg"/ alt="{l s='Add to cart'}">
										</a>
										{/if}

									</div>
								</div>




								<div class="showSecond">


									<ul>


										<li>
											<div class="wishlist">
												<a class="addToWishlist wishlistProd_{$categoryProduct.id_product|intval}" href="#" rel="{$categoryProduct.id_product|intval}" onclick="WishlistCart('wishlist_block_list', 'add', '{$categoryProduct.id_product|intval}', false, 1); return false;">
													<span>{l s="Save" mod="productscategory"}</span>
												</a>
											</div>
										</li>

										
										
										<li>



											{if !$PS_CATALOG_MODE && ($categoryProduct.allow_oosp || $categoryProduct.quantity > 0)}

											<a class="ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$categoryProduct.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$categoryProduct.id_product|intval}" title="{l s='Add to cart' mod='productscategory'}">

												<span>{l s="Kaufen" mod="productscategory"}</span>

											</a> 

											{else}
											

											<a class="ajax_add_to_cart_button disabled">
												<span>{l s="Nicht verfügbar" mod="productscategory"}</span>


											</a>
											{/if}


										</li>

										

									</ul>


								</div>



							</div>




						</div>




						
					</div>

				</li>
				{/foreach}
			</ul>
		</div>
	</section>
	{/if}
