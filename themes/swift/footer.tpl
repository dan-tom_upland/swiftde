{*
	* 2007-2016 PrestaShop
	*
	* NOTICE OF LICENSE
	*
	* This source file is subject to the Academic Free License (AFL 3.0)
	* that is bundled with this package in the file LICENSE.txt.
	* It is also available through the world-wide-web at this URL:
	* http://opensource.org/licenses/afl-3.0.php
	* If you did not receive a copy of the license and are unable to
	* obtain it through the world-wide-web, please send an email
	* to license@prestashop.com so we can send you a copy immediately.
	*
	* DISCLAIMER
	*
	* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
	* versions in the future. If you wish to customize PrestaShop for your
	* needs please refer to http://www.prestashop.com for more information.
	*
	*  @author PrestaShop SA <contact@prestashop.com>
	*  @copyright  2007-2016 PrestaShop SA
	*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
	*  International Registered Trademark & Property of PrestaShop SA
	*}
	{if !isset($content_only) || !$content_only}
</div><!-- #center_column -->
{if isset($right_column_size) && !empty($right_column_size)}
<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
{/if}
</div><!-- .row -->
</div><!-- #columns -->
</div><!-- .columns-container -->
{if isset($HOOK_FOOTER)}
<!-- Footer -->
<div class="footer-container">
	<footer id="footer">
		<div class="firstLine">
			<div class="container">
				<div class="leftSide">
					{hook h="displayFooter" mod="blocksocial"}
				</div>			
				<div class="rightSide">
					<img src="{$img_dir}/custom/6-payments-method.jpg" class="paymentsMethod" alt="payments method" />
				</div>
			</div>
		</div>
		<div class="secondLine">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-xs-12 col-sm-4">
						{hook h="displayFooter" mod="blockcategories"}
					</div>
					<div class="col-md-2 col-xs-12 col-sm-4">
						{hook h="displayFooter" mod="blockcms"}
					</div>
					<div class="col-lg-2 col-xs-12 col-md-3 col-sm-6">
						{hook h="displayLeftColumn" mod="blockcms"}
					</div>
					<div class="col-md-2 col-xs-12 col-sm-4">
						{hook h="displayFooter" mod="blockmyaccountfooter"}
					</div>					
					<!-- <div class="col-lg-2 col-xs-12 col-md-3 col-sm-6"> -->
					{* hook h="displayFooter" mod="blockcontactinfos" *}
					<!-- </div>		 -->
					<div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
						{hook h="displayFooter" mod="blocknewsletter"}
					</div>	
				</div>
			</div>
		</div>
	</footer>
</div><!-- #footer -->
{/if}
</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}

<script type="text/javascript">

	$(document).ready(function() {
		if (!!$.prototype.bxSlider)
			$('.bxslider-hometabsnew').bxSlider({
				wrapperClass: 'bxwrapper-hometabsnew',
				minSlides: 1,
				maxSlides: 8,
				slideWidth: 300,
				slideMargin: 0,
				pager: false,
				controls: true,
				nextText: 'Next',
				prevText: 'Prev',
				auto: true,
				autoHover: true,
				autoPlay: true,
				autoStart: true,
				speed: 900,
				moveSlides:1,
				pause: 3200,
				infiniteLoop: true,
				hideControlOnEnd: false
			});
	});
</script>


</body>
</html>